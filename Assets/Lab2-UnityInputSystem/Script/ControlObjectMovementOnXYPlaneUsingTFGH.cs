using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObjectMovementOnXYPlaneUsingTFGH : MonoBehaviour
{
    public float _movementStep;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F))
        {
            this.transform.Translate(-_movementStep , 0, 0);
        }
        else if (Input.GetKeyUp(KeyCode.H))
        {
            this.transform.Translate(_movementStep , 0, 0);
        }
        else if (Input.GetKeyUp(KeyCode.T))
        {
            this.transform.Translate(0, _movementStep , 0);
        }
        else if (Input.GetKeyUp(KeyCode.G))
        {
            this.transform.Translate(0, -_movementStep , 0);
        }
    }
}
