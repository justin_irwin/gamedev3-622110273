using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Content;
using UnityEngine;

public class ControlObjectUsingMouse : MonoBehaviour
{
    private Vector3 _mousePrevPosition;
    public float _mouseDeltaVectorScaling = 0.5f;
    public float _mouseScrollDelta = 10f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouseCurrentPos = Input.mousePosition;
        Vector3 mouseDeltaVector = Vector3.zero;
        mouseDeltaVector = (mouseCurrentPos - _mousePrevPosition).normalized;
        

        if (Input.GetMouseButton(0))
        {
            this.transform.Translate(mouseDeltaVector * _mouseDeltaVectorScaling, Space.World);
        }
        this.transform.Translate(0, 0, Input.mouseScrollDelta.y * _mouseDeltaVectorScaling, Space.World);

        if (Input.GetMouseButtonDown(1)&Input.GetMouseButtonUp(1))
        {
           print("Position hahahahah");
        }
        
        _mousePrevPosition = mouseCurrentPos;
    }
}
