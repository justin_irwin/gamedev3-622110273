using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardBackwardAlongXAxisMovement : MonoBehaviour
{
    public float MAX_MOVEMENT_DISTANCE = 2.0f;

    float _displacementCounter = 0;
    
    [SerializeField] private float _xComponentSpeed = 0.02f;

    Vector3 _movementSpeed = Vector3.zero;
    
    void Start()
    {
        _movementSpeed.x = _xComponentSpeed;
    }

    
    void Update()
    {
        this.transform.position += _movementSpeed;

        _displacementCounter += _movementSpeed.x;

        if (Mathf.Abs(_displacementCounter) > MAX_MOVEMENT_DISTANCE)
        {
            _displacementCounter = 0;
            _movementSpeed *= -1;
        }
    }
}
