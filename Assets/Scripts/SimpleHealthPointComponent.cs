using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHealthPointComponent : MonoBehaviour
{
    [SerializeField] public const float MAX_HP = 100;

    [SerializeField] public float _healthPoint;
    
    //Property
    public float HealthPoint
    {
        get
        {
            return _healthPoint;
        }
        set
        {
            if (value > 0)
            {
                _healthPoint = value;
            }
            else
            {
                _healthPoint = MAX_HP;
            }
        }
    }
}
