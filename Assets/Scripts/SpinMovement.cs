using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;

public class SpinMovement : MonoBehaviour
{
    [SerializeField] private float _angularSpeed = 5.0f;
    [SerializeField] private Vector3 _axis0fRotation = new Vector3(1.0f, 0, 0);

    Transform _objTransform;
    void Start()
    {
        _objTransform = this.gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        _objTransform.Rotate(_axis0fRotation, _angularSpeed);
    }
}
